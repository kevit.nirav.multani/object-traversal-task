const fs = require('fs')
const array = require('./utils/readArray')
const convert = require('./utils/convertToJson')

let convertedData = convert(array)

convertedData = JSON.stringify(convertedData)
fs.writeFileSync('./src/jsonOutput.json', convertedData)
console.log('Data converted successfully \nTo see the results open jsonOutput file in src directory')