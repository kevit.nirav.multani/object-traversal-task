
const convertTOJson = (arr) => {
    let out = {}


    arr.forEach(({ Country, Department, Experience, "Open Jobs": openJobs, Image, Title, Location, Description, Link }) => {

        let RF = out



        if (!RF[`${Country}`]) {
            RF[`${Country}`] = {}
        }

        RF = RF[`${Country}`]




        if (!RF.total) {
            RF.total = 0
        }
        RF.total += openJobs

        if (!RF[`${Department}`]) {
            RF[`${Department}`] = {}
        }

        RF = RF[`${Department}`]





        if (!RF[`${Experience}`]) {
            RF[`${Experience}`] = {}
        }

        if (!RF.total) {
            RF.total = 0
        }
        RF.total += openJobs

        RF = RF[`${Experience}`]






        if (!RF.total) {
            RF.total = 0
        }
        RF.total += openJobs

        if (!RF.jobs) {
            RF.jobs = []
        }

        RF = RF.jobs

        RF.push({
            Image,
            Title,
            Location,
            Description,
            Link,
            "Open Jobs": openJobs
        })


    })

    return out
}

module.exports = convertTOJson