const fs = require('fs')
const path = require('path')

const filepath = path.join(__dirname, '../src/arrayInput.txt')
let data = fs.readFileSync(filepath)
data = data.toString() //binary to string 
data = JSON.parse(data)  //string to arr of obj
module.exports = data
